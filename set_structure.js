module.exports = [
	{key: "collection", name: `标记-采集物品`, type: "select",
		options: [
			{name: "清除", key: ['clean']},
			{name: "杂草", key: [1]}, // 坚韧的杂草
			{name: "植物", key: [2,3,4,5,6]}, // 野生玉米 野生红葡萄 黄蘑菇 老南瓜 苹果树
			{name: "岩石", key: [101]}, // 岩石 
			{name: "矿石", key: [102,103,104,105,106]}, // 钴矿石 硒矿石 水晶矿石 秘银矿石 碣矿石
			{name: "无色", key: [201]}, // 无色结晶
			{name: "精气", key: [202,203,204,205,206]}, // 赤色结晶 绿色结晶 青色结晶 白色结晶 被污染的花
		]
	},
	{key: "deadMember", name: `标记-队友尸体`, type: "bool"},
	{key: "creature", name: `标记生物(set_custom.js)`, type: "bool"},
	{key: "logNPC", name: `查询NPCs`, type: "bool"},
	{key: "h_ID", name: `huntingZoneId →`, type: "number", min: 1.0, max: 99999999.0, step: 1.0},
	{key: "t_ID", name: `templateId →`, type: "number", min: 1.0, max: 99999999.0, step: 1.0},
	{key: "log", name: `日志`, type: "bool"},
]
