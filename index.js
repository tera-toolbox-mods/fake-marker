const fs = require('fs')
const path = require("path")
const UI = require('tera-mod-ui').Settings
exports.NetworkMod = function(mod) {
	// UI设置
	if (!global.TeraProxy.GUIMode) return
	let ui = new UI(mod, require('./set_structure'), mod.settings, {height: 220})
	ui.on('update', settings => {
		if (mod.settings.collection=="clean" && marks.size!=0) removeAllMarks(marks)
		if (!mod.settings.deadMember) removeAllMarks(marks)
		if (!mod.settings.creature) removeAllMarks(marks)
		
		mod.send('C_SET_VISIBLE_RANGE', 1, { range: 1 })
		mod.setTimeout(() => {
			mod.send('C_SET_VISIBLE_RANGE', 1, { range: visibleRange })
		}, 1000)
		
		delete require.cache[require.resolve('./set_config.js')]
		set_config = require('./set_config.js')
	})
	this.destructor = () => { if (ui) ui.close(), ui = null, removeAllMarks(marks) }
	mod.command.add(["mark", "marker", "标记"], () => ui.show())
	
	let set_config = require('./set_config.js'), partyMembers = [], visibleRange = 2500, marks = new Map()
	mod.game.initialize('me')
	mod.hookOnce('C_SET_VISIBLE_RANGE', 1, e => {
		visibleRange = e.range
	})
	mod.hook('S_SPAWN_COLLECTION', 4, e => {
		if (!mod.settings.collection.split(',').includes(`${e.id}`)) return
		SpawnMark(e.gameId*100n, e.loc)
		mod.queryData('/StrSheet_Collections/String@collectionId=?', [e.id]).then(result => {
			if (!result || !result.attributes || !result.attributes.string) return
			SysMessage(result.attributes.string, `采集物`)
			log(`add marks: ${marks.size} | coll ${e.id} |${result.attributes.string}`)
		})
	})
	mod.hook('S_DESPAWN_COLLECTION', 2, e => {
		if (marks.has(e.gameId*100n)) RemoveMark(e.gameId*100n)
	})
	
	mod.hook('S_SPAWN_NPC', 11, e => {
		if (!mod.settings.creature) return
		if (set_config.tipNPCs.find(res => res.h_Id==e.huntingZoneId && res.t_Id==e.templateId)) {
			SpawnMark(e.gameId*100n, e.loc)
			logCreature(e.huntingZoneId, e.templateId)
		}
		if (mod.settings.h_ID==e.huntingZoneId && mod.settings.t_ID==e.templateId) {
			SpawnMark(e.gameId*100n, e.loc)
			logCreature(e.huntingZoneId, e.templateId)
		}
		if (!mod.settings.logNPC) return
		mod.queryData('/StrSheet_Creature/HuntingZone@id=?/String@templateId=?', [e.huntingZoneId, e.templateId]).then(result => {
			if (!result || !result.attributes || !result.attributes.name) return
			mod.command.message(`npc ${e.huntingZoneId}_${e.templateId} | "${result.attributes.name}`)
		})
	})
	function logCreature(h_ID, t_ID) {
		mod.queryData('/StrSheet_Creature/HuntingZone@id=?/String@templateId=?', [h_ID, t_ID]).then(result => {
			if (!result || !result.attributes || !result.attributes.name) return
			SysMessage(result.attributes.name, result.attributes.title)
			log(`add marks: ${marks.size} | npc ${h_ID}_${t_ID} | "${result.attributes.name}`)
		})
	}
	mod.hook('S_DESPAWN_NPC', 3, e => {
		if (marks.has(e.gameId*100n)) RemoveMark(e.gameId*100n)
	})
	
	mod.hook('S_DEAD_LOCATION', 2, e => {
		if (!mod.settings.deadMember) return
		partyMembers.forEach(member => {
			if (member.gameId != e.gameId || mod.game.me.is(e.gameId)) return
			SpawnMark(member.playerId, e.loc)
			log(`add marks: ${marks.size} | dead ${member.playerId} | ${member.name}`)
		})
	})
	mod.hook('S_SPAWN_USER', 16, e => {
		if (!mod.settings.deadMember || partyMembers.length==0) return
		partyMembers.forEach(member => {
			if (member.playerId != e.playerId || e.alive) return
			SpawnMark(member.playerId, e.loc)
			log(`add marks: ${marks.size} | user ${member.playerId} | ${member.name}`)
		})
	})
	mod.hook('S_DESPAWN_USER', 3, e => {
		partyMembers.forEach(member => {
			if (member.gameId == e.gameId) RemoveMark(member.playerId)
		})
	})
	mod.hook('S_PARTY_MEMBER_LIST', 8, e => {
		partyMembers = e.members
	})
	mod.hook('S_PARTY_MEMBER_STAT_UPDATE', 3, e => {
		if (marks.has(e.playerId) && e.alive) RemoveMark(e.playerId)
	})
	mod.hook('S_BAN_PARTY_MEMBER', 1, e => {
		if (marks.has(e.playerId)) RemoveMark(e.playerId)
		partyMembers = partyMembers.filter(p => p.playerId != e.playerId)
	})
	mod.hook('S_LOGOUT_PARTY_MEMBER', 1, e => {
		if (marks.has(e.playerId)) RemoveMark(e.playerId)
		partyMembers = partyMembers.filter(p => p.playerId != e.playerId)
	})
	mod.hook('S_LEAVE_PARTY_MEMBER', 2, e => {
		if (marks.has(e.playerId)) RemoveMark(e.playerId)
		partyMembers = partyMembers.filter(p => p.playerId != e.playerId)
	})
	mod.hook('S_LEAVE_PARTY', 1, e => {
		partyMembers.forEach(member => { RemoveMark(member.playerId) })
		partyMembers = []
	})
	
	function SpawnMark(id, loc, item = 88704) {
		marks.set(id, loc)
		loc.z = loc.z-80,
		mod.send('S_SPAWN_DROPITEM', 9, {
			gameId: id,
			loc: loc,
			item: item,
			amount: 1
		})
	}
	function RemoveMark(id) {
		marks.delete(id)
		mod.send('S_DESPAWN_DROPITEM', 4, { gameId: id })
		log(`del: ${marks.size}`)
	}
	function removeAllMarks(array) {
		array.forEach((value, keys) => { RemoveMark(keys) })
		array.clear()
	}
	function SysMessage(msg, title) {
		mod.send('S_CUSTOM_STYLE_SYSTEM_MESSAGE', 1, { style: 44, message: `<font size="30">${title} ${msg}</font>` })
		mod.command.message(`分流-${mod.game.me.channel} ${title} ${msg}`)
	}
	function log(msg) {
		if (mod.settings.log) mod.command.message(msg)
	}
	
	mod.game.on('enter_loading_screen', () => { removeAllMarks(marks) })
	mod.game.on('leave_game', () => { removeAllMarks(marks) })
}
